describe('Home Tests', () => {
  it('Visits the Petclinic and check contents', () => {
    cy.visit('http://localhost:4200/petclinic/')
    cy.contains('Home')
    cy.contains('Owners')
    cy.contains('Veterinarians')
    cy.contains('Pet Type')
    cy.contains('Specialties')
  })

})

describe('Owner Tests', () => {
  it('Visits the Petclinic', () => {
    cy.visit('http://localhost:4200/petclinic/')
  })
  it('Retrieves owners', () => {
      cy.contains('Owners').click()
      cy.contains('All').click()
      cy.url().should('include', '/owners')
  })
  it('Check add Owner', () => {
      cy.contains('Add Owner').click()

      cy.url().should('include', '/owners/add')

      cy.get('#firstName')
            .type('FirstNameTest')
            .should('have.value', 'FirstNameTest')

      cy.get('#lastName')
                    .type('OwnerLastNameTest')
                    .should('have.value', 'OwnerLastNameTest')
      cy.get('#address')
                    .type('Owner Adress Test')
                    .should('have.value', 'Owner Adress Test')
      cy.get('#city')
                    .type('OwnerCityTest')
                    .should('have.value', 'OwnerCityTest')
      cy.get('#telephone')
                    .type('0205548996')
                    .should('have.value', '0205548996')

      cy.contains('Add Owner').click()
      cy.url().should('include', '/owners')
  })



})


describe('Veterinarians Tests', () => {
  it('Visits the Petclinic', () => {
      cy.visit('http://localhost:4200/petclinic/')
    })


  it('Add Veterinarians', () => {
    cy.contains('Veterinarians').click()
    cy.contains('Add New').click()

    cy.url().should('include', '/vets/add')

    cy.get('#firstName')
                .type('VetFirstNameTest')
                .should('have.value', 'FirstNameTest')

    cy.get('#lastName')
                  .type('VetLastNameTest')
                  .should('have.value', 'LastNameTest')
    cy.get('#type')
                  .type('d')
                  .should('have.value', 'dentistry')
  })



})


