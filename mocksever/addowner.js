var mockServerClient = require('mockserver-client').mockServerClient;
mockServerClient("localhost", 9966).mockAnyResponse({
    "id": "Add Owner",
    "httpRequest": {
        "method": "POST",
        "path": "/petclinic/api/owners",
        "body": {
             "id": null,
             "firstName" : "Eve",
             "lastName" : "Miller",
             "address" : "31 Rue Richelieu",
             "city" : "Tours",
             "telephone" : "0662892309"
        }
    },                                                    
    "times": {
        "remainingTimes": 1,
        "unlimited": false
    },
    "timeToLive": {
        "timeUnit": "SECONDS",
        "timeToLive": 60,
        "unlimited": false
    },
    "httpResponse": {
        "statusCode": 202,
        "body": "Insert a new owner into database",
        "delay": {
            "timeUnit": "SECONDS",
            "value": 10
        }
    }
//    "httpResponseClassCallback": {
//        "callbackClass": "org.mockserver.examples.mockserver.CallbackActionExamples$TestExpectationResponseCallback"
//    }
}).then(
    function () {
        console.log("expectation created");
    },
    function (error) {
        console.log(error);
    }
);